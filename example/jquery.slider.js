;(function ( $ ) {
  $.fn.slider = function () {
    var $this = $(this),
        slides = $(this).children('.slide'),
        firstSlide = slides.first(),
        lastSlide = slides.last(),
        duration = 500;
				
    // Append navigation
    $this.prepend('<a style id="show-prev-slide" >prev</a><a id="show-next-slide" >next</a>');
    
    // CSS
    $this.css({ 'display' : 'block' });
    
    function changeSlide( current, next ) {
      current.animate({"opacity" : "0", 'z-index' : 0}, duration).removeClass('showen');
      next.animate({'opacity' : "1", 'z-index' : 1}, duration).addClass('showen');
    }
    
    function initSlider() {  
      /** 
       * Взять картинки из тегов img и установить их как фоновое изображение блокам.
       * Это делается для того, чтобы можно было проще управлять размером картинки и их позиционированием.
       * А так же задать всем картинкам один размер, т.к. они могут именять разный размер.
       */
      slides.each(function (a) {
        this.setAttribute('id',a+1);
        this.setAttribute('style', 'background-image: url('+this.firstChild.getAttribute('src')+');');
      });
      // Плавно показать первый слайд.
      firstSlide.animate( { 'opacity' : '1', 'z-index' : '1' }, duration/2 );
      // Отметить первый слайд как показанный.
      firstSlide.addClass('showen');
    };
    
    // Actions
    $(window).load( function () {
      initSlider();
    });
				
    $('#show-next-slide').click( function () {
      /**
       * showen - определить показанный слайд.
       * next - определить следующий слайд.
       */
      var showen = slides.siblings('.showen'),
          next = showen.next('.slide');
      /**
       * Если показан последний слайд, то следующий будет первым.
       * Иначе просто переключить на следующий слайд.
       */    
      if (parseInt(showen.attr("id")) == 3) {
        changeSlide(showen, firstSlide);
      } else {
        changeSlide(showen, next);
      }
    });
				
    $('#show-prev-slide').click( function () {
      /**
       * showen - определить показанный слайд.
       * prev - определить предидущий слайд.
       */
      var showen = slides.siblings('.showen'),
        prev = showen.prev('.slide');
      /**
       * Если показан первый слайд, то следующий будет последним.
       * Иначе просто переключить на предидущий слайд.
       */ 
      if (parseInt(showen.attr("id")) == 1) {
        changeSlide(showen, lastSlide);
      } else {
        changeSlide(showen, prev);
      }          
    });
  }
})( jQuery );